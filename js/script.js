"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(name) {
        this._name = name;
    }

    get name() {
        return this._name;
    }

    set age(age) {
        this._age = age;
    }

    get age() {
        return this._age;
    }

    set salary(salary) {
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary * 3;
    }
}

let user1 = new Programmer("Vlad", 26, 10000, ["russian", "english", "ukrainian"]);
let user2 = new Programmer("Olga", 28, 30000, ["Ukrainian", "english", "spanish"]);
let user3 = new Programmer("Max", 18, 6000, ["Ukrainian", "english", "french"]);
console.log(user1);
console.log(user2);
console.log(user3);